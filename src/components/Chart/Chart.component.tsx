import { ChartProps, ChartType, SubChartProps } from '../../interfaces/Chart';
import { ResponsiveContainer } from 'recharts';
import BarChartComponent from './ChartSubTypes/BarChart.component';
import RadarChartComponent from './ChartSubTypes/RadarChart.component';
import StackedChartComponent from './ChartSubTypes/StackedChart.component';

export default function ChartComponent(props: ChartProps) {
    const {keysWithColors, data} = props;
    const reversedArray = [...data].reverse();

    const mappedData = reversedArray.map(covidDayData => {
        return {
            ...covidDayData,
            lastUpdatedAtApify: covidDayData.lastUpdatedAtApify.substr(0, 10)
        };
    });

    const subChartProps: SubChartProps = {data: mappedData, keysWithColors};
    let chart;
    switch (props.chartType) {
        case ChartType.BAR:
            chart = BarChartComponent(subChartProps);
            break;
        case ChartType.RADAR:
            chart = RadarChartComponent(subChartProps);
            break;
        case ChartType.STACKED:
            chart = StackedChartComponent(subChartProps);
            break;
        default:
            chart = (<div/>)
            break;
    }

    return (
        <div>
            {props.chartType === ChartType.RADAR ? <h4>This chart shows info only from the latest data.</h4> : ''}
            <ResponsiveContainer aspect={2}>
                {chart}
            </ResponsiveContainer>
        </div>
    )
}
