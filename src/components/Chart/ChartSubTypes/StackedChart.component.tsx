import { SubChartProps } from '../../../interfaces/Chart';
import { Area, AreaChart, CartesianGrid, Tooltip, XAxis, YAxis } from 'recharts';

export default function StackedChartComponent(props: SubChartProps) {
    const {data, keysWithColors} = props;

    return (
        <AreaChart
            data={data}
            margin={{
                top: 10,
                right: 30,
                left: 0,
                bottom: 0,
            }}
        >
            <CartesianGrid strokeDasharray="3 3"/>
            <XAxis dataKey="lastUpdatedAtApify"/>
            <YAxis width={100}/>
            <Tooltip/>
            {
                keysWithColors.map((entry) => {
                    return (<Area type="monotone" key={entry.key} dataKey={entry.key} fill={entry.color}
                                  stroke={entry.color} stackId={keysWithColors.indexOf(entry)}/>);
                })
            }
        </AreaChart>
    );
}
