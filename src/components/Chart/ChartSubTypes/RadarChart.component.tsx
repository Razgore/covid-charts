import { SubChartProps } from '../../../interfaces/Chart';
import { PolarAngleAxis, PolarGrid, PolarRadiusAxis, Radar, RadarChart } from 'recharts';

export default function RadarChartComponent(props: SubChartProps) {
    const {data, keysWithColors} = props;
    const lastData = data[data.length - 1];

    const mappedData: Array<{ name: string, value: number }> = [];

    keysWithColors.forEach(keyWithColor => {
        mappedData.push(
            // @ts-ignore
            {name: keyWithColor.key, value: lastData[keyWithColor.key]}
        )
    });

    return (
        <RadarChart cx="50%" cy="50%" outerRadius="80%" data={mappedData}>
            <PolarGrid/>
            <PolarAngleAxis dataKey="name"/>
            <PolarRadiusAxis/>
            <Radar name="covid" dataKey="value" stroke="#8884d8" fill="#8884d8" fillOpacity={0.6}/>
        </RadarChart>
    )
}
