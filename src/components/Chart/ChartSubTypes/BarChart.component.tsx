import { SubChartProps } from '../../../interfaces/Chart';
import { Bar, BarChart, CartesianGrid, Legend, Tooltip, XAxis, YAxis } from 'recharts';

export default function BarChartComponent(props: SubChartProps) {
    const {data, keysWithColors} = props;

    return (
        <BarChart
            data={data}
            margin={{
                top: 5,
                right: 30,
                left: 20,
                bottom: 5,
            }}
        >
            <CartesianGrid strokeDasharray="3 3"/>
            <XAxis dataKey="lastUpdatedAtApify"/>
            <YAxis/>
            <Tooltip/>
            <Legend/>
            {
                keysWithColors.map((entry) => {
                    return (<Bar key={entry.key} dataKey={entry.key} fill={entry.color}/>);
                })
            }
        </BarChart>
    );
}
