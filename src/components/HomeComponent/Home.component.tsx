import { useAppDispatch, useAppSelector } from '../../redux/hooks';
import {
    chartTypeSelected,
    dataTypeSelected,
    fetchLimitChanged,
    getChartType,
    getCovidDataAsync,
    getCurrentCovidData,
    getDataTypes,
    getFetchLimit,
    getSelectedDataTypes
} from '../../redux/HomeComponent/homePageSlice';
import { useEffect } from 'react';
import ChartComponent from '../Chart/Chart.component';
import { ChartType, KeyWithColor } from '../../interfaces/Chart';
import Select from 'react-select';
import { Dropdown } from '../../interfaces/Common';

export function HomeComponent() {
    const dispatch = useAppDispatch();
    const chartType = useAppSelector(getChartType);
    const data = useAppSelector(getCurrentCovidData);
    const limit = useAppSelector(getFetchLimit);
    const selectedDataTypes = useAppSelector(getSelectedDataTypes);
    const dataTypes = useAppSelector(getDataTypes);

    useEffect(() => {
        dispatch(getCovidDataAsync(limit));
    }, [dispatch, limit]);

    const keysWithColors: Array<KeyWithColor> = [
        {key: 'infected', color: '#ffbd90'},
        {key: 'deceased', color: '#000000'},
        {key: 'recovered', color: '#82ca9d'},
        {key: 'quarantined', color: '#ff6962'},
    ];

    const dataTypeKeys = selectedDataTypes.map(type => type.value);

    const filteredKeysWithColors = keysWithColors.filter(keyWithCol => dataTypeKeys.find(key => key === keyWithCol.key));

    const chartTypeOptions: Array<Dropdown> = [
        {value: '0', label: 'Bar'},
        {value: '1', label: 'Radar'},
        {value: '2', label: 'Stacked'},
    ];

    return (
        <div className="container-fluid">
            <div className="row justify-content-center">
                <h2>Hungarian Covid Charts</h2>
            </div>
            <div className="row justify-content-center">
                <div className="col-12 col-md-4">
                    <label>
                        Number of items:
                    </label>
                    <input type="number" className="form-control" placeholder="Number of items" min={1} max={100}
                           onChange={(event => dispatch(fetchLimitChanged(+event.target.value)))} defaultValue={limit}/>
                </div>
                <div className="col-12 col-md-4">
                    <label>
                        Chart type:
                    </label>
                    <Select
                        options={chartTypeOptions}
                        onChange={(event => dispatch(chartTypeSelected(event ? event.value : ChartType.NONE.toString())))}
                        placeholder={"Choose a chart type"}
                    />

                </div>
                <div className="col-12 col-md-4">
                    <label>
                        Data Type:
                    </label>
                    <Select
                        isMulti={true}
                        options={dataTypes}
                        defaultValue={selectedDataTypes}
                        placeholder={"Select multiple data types"}
                        onChange={(event => dispatch(dataTypeSelected(event ? Array.from(event) : [])))}
                    />
                </div>

            </div>
            <div className="row justify-content-center">
                <div className="col-12 col-md-8">
                    <div className="row">
                        <ChartComponent keysWithColors={filteredKeysWithColors} chartType={chartType} data={data}/>
                    </div>
                </div>
            </div>
        </div>
    );
}
