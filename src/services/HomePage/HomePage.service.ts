import { ApifyResponseWrapper } from '../../interfaces/CovidData';

export default abstract class HomePageService {
    abstract getHungarianCovidData(limit: number): Promise<ApifyResponseWrapper>;
}
