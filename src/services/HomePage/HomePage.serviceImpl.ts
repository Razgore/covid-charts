import HomePageService from './HomePage.service';
import { ApifyResponseWrapper } from '../../interfaces/CovidData';
import axios from 'axios';
import { DatasetKeys } from '../../api/DatasetKeys';

export default class HomePageServiceImpl extends HomePageService {
    public async getHungarianCovidData(limit: number): Promise<ApifyResponseWrapper> {
        return await axios.get(`https://api.apify.com/v2/datasets/${DatasetKeys.HUNGARY}/items?limit=${limit}&desc=true`);
    }
}
