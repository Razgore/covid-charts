import HomePageService from './HomePage/HomePage.service';
import HomePageServiceImpl from './HomePage/HomePage.serviceImpl';

export const homePageService: HomePageService = new HomePageServiceImpl();
