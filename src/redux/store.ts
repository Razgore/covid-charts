import { Action, configureStore, ThunkAction } from '@reduxjs/toolkit';
import { homeSlice } from './HomeComponent/homePageSlice';

export const store = configureStore({
  reducer: {
    home: homeSlice.reducer
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType,
    RootState,
    unknown,
    Action<string>>;
