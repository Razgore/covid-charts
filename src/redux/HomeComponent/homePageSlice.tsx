import { HomePageState } from '../../interfaces/HomePage';
import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { homePageService } from '../../services/service';
import { RootState } from '../store';
import { ChartType } from '../../interfaces/Chart';
import { Dropdown } from '../../interfaces/Common';

const initialState: HomePageState = {
    data: [],
    status: '',
    limit: 5,
    selectedChartType: ChartType.NONE,
    dataTypes: [
        {value: 'infected', label: 'infected'},
        {value: 'deceased', label: 'deceased'},
        {value: 'recovered', label: 'recovered'},
        {value: 'quarantined', label: 'quarantined'},
    ],
    selectedDataTypes: [
        {value: 'infected', label: 'infected'},
        {value: 'deceased', label: 'deceased'},
        {value: 'recovered', label: 'recovered'},
        {value: 'quarantined', label: 'quarantined'},
    ],
};

export const getCovidDataAsync = createAsyncThunk(
    'home/getCovidData',
    async (limit: number) => {
        const response = await homePageService.getHungarianCovidData(limit);
        return response.data;
    }
)


export const homeSlice = createSlice({
    name: 'home',
    initialState,
    reducers: {
        chartTypeSelected: (state, action: PayloadAction<string>) => {
            state.selectedChartType = +action.payload
        },
        dataTypeSelected: (state, action: PayloadAction<Array<Dropdown>>) => {
            state.selectedDataTypes = action.payload
        },
        fetchLimitChanged: (state, action: PayloadAction<number>) => {
            state.limit = action.payload
        },
    },
    extraReducers: builder => {
        builder
            .addCase(getCovidDataAsync.pending, state => {
                state.status = 'loading';
            })
            .addCase(getCovidDataAsync.fulfilled, (state, action) => {
                state.status = 'idle';
                state.data = action.payload;
            })
            .addCase(getCovidDataAsync.rejected, (state) => {
                state.status = 'failed';
            });
    },
});

export const {chartTypeSelected, dataTypeSelected, fetchLimitChanged} = homeSlice.actions;
export const getCurrentCovidData = (state: RootState) => state.home.data;
export const getFetchLimit = (state: RootState) => state.home.limit;
export const getChartType = (state: RootState) => state.home.selectedChartType;
export const getSelectedDataTypes = (state: RootState) => state.home.selectedDataTypes;
export const getDataTypes = (state: RootState) => state.home.dataTypes;
export default homeSlice.reducer;
