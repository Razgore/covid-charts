export interface CovidData {
    activeInfected: number;
    deceased: number;
    infected: number;
    lastUpdatedAtApify: string;
    lastUpdatedAtSource: string;
    quarantined: number;
    readMe: string;
    recovered: number;
    sourceUrl: string;
    tested: number;
}

export interface ApifyResponseWrapper {
    data: Array<CovidData>;
}
