import { CovidData } from './CovidData';

export interface ChartProps {
    data: Array<CovidData>;
    chartType: ChartType;
    keysWithColors: Array<KeyWithColor>;
}

export interface SubChartProps {
    data: Array<CovidData>;
    keysWithColors: Array<KeyWithColor>;
}

export enum ChartType {
    BAR, RADAR, STACKED, NONE
}

export interface KeyWithColor {
    key: string;
    color: string;
}
