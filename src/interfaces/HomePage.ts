import { CovidData } from './CovidData';
import { ChartType } from './Chart';
import { Dropdown } from './Common';

export interface HomePageState {
    data: Array<CovidData>;
    status: string;
    limit: number;
    selectedChartType: ChartType;
    dataTypes: Array<Dropdown>;
    selectedDataTypes: Array<Dropdown>;
}
