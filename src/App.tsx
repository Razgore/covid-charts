import React from 'react';
import './App.css';
import { HomeComponent } from './components/HomeComponent/Home.component';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
    return (
        <div className="App">
            <HomeComponent/>
        </div>
    );
}

export default App;
